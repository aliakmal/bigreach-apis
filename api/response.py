from django.shortcuts import render, HttpResponse, render_to_response, HttpResponseRedirect
import json


class Response(object):
  def success(self, data, message='Action completed successfull'):
    return HttpResponse(json.dumps({'success':'1', 'status': 'success', 'message':message, 'data':data}, cls=json.JSONEncoder, indent=2), content_type="application/json", status=200)

  def error(self, data, message='There was an error'):
    return HttpResponse(json.dumps({'success':'0', 'status': 'error', 'message':message, 'data':data}, cls=json.JSONEncoder, indent=2), content_type="application/json", status=200)