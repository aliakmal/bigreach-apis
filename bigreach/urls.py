"""bigreach URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.views.decorators.csrf import csrf_exempt
from api.views import *


urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^api/users/auth/$', csrf_exempt(loginUser), name='loginUser'),
    url(r'^api/users/get-confirmable-account/$', csrf_exempt(getConfirmableUser), name='getConfirmableUser'),
    url(r'^api/users/confirm-account/$', csrf_exempt(confirmUser), name='getConfirmableUser'),
    url(r'^api/call-logs/$', csrf_exempt(getCallLogs), name='getCallLogs'),
    url(r'^api/test/$', csrf_exempt(testUsers), name='testUsers'),
    url(r'^api/dashboard/$', csrf_exempt(getHomeBoard), name='getHomeBoard'),
    url(r'^api/facebook/analytics$', csrf_exempt(getFacebookBoard), name='getFacebookBoard'),

]

