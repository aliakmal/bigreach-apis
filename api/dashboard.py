from django.shortcuts import render, HttpResponse, render_to_response, HttpResponseRedirect
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core.paginator import Paginator

from django.db import connection
from api.models import *
import json
import uuid
import urllib
import bcrypt
from api.response import Response
import requests

import string
from django.utils.dateparse import parse_date
import random
from datetime import datetime, timedelta
import pprint
pp = pprint.PrettyPrinter(indent=6)
rh = Response() #response handler


class Dashboard(object):
  def getHomeBoard(self, usrID, since, until):
    merchant_ids  = MerchantUser.objects.filter(user_id=usrID).values_list('merchant_id', flat=True)
    venue_ids     = BusinessMerchant.objects.filter(merchant_id__in=merchant_ids).values_list('business_id', flat=True)
    venues        = Businesses.objects.filter(id__in=venue_ids)
    merchants     = Merchants.objects.filter(id__in=merchant_ids)
    facebook_pages = ProviderPage.objects.filter(merchant_id__in=merchant_ids)

    result = {'pages':[], 'venues':[], 'frm':'', 'to':''}

    for page in facebook_pages:
      token = Token.objects.get(id = page.token_id)
      msg = {'access_token':token.token, 'since':since, 'until':until }

      url = "https://graph.facebook.com/v2.8/{0}/insights/page_views_total".format(page.page_provider_id)
      page_views = requests.get(url, params=msg)

      url = "https://graph.facebook.com/v2.8/{0}/insights/page_fan_adds".format(page.page_provider_id)
      page_fan_likes = requests.get(url, params=msg)

      url = "https://graph.facebook.com/v2.8/{0}/insights/page_consumptions".format(page.page_provider_id)
      page_engagement = requests.get(url, params=msg)

      result['pages'].append({ 'page_id': page.id,
                            'fb_id':page.page_provider_id, 
                            'title':page.title,
                            'page_views_total':self.getGraphTotalFromFBResultset(page_views.json()),
                            'page_fan_likes_total':self.getGraphTotalFromFBResultset(page_fan_likes.json()),
                            'page_engagement_total':self.getGraphTotalFromFBResultset(page_engagement.json()) })
    
    for venue in venues:
      missed_calls = CallLogs.objects.filter(business_id=venue.id, call_transfer_status='Missed').all().count()
      connected_calls = CallLogs.objects.filter(business_id=venue.id, call_transfer_status='Connected').all().count()
      result['venues'].append({ 
                              'venue_id': venue.id, 
                              'name': venue.name, 
                              'missed_calls': missed_calls, 
                              'connected_calls':connected_calls
                            })

    result['frm'] = datetime.strftime(datetime.fromtimestamp(since), "%Y-%m-%d")
    result['to'] = datetime.strftime(datetime.fromtimestamp(until), "%Y-%m-%d")

    return rh.success(result, 'Success')


  def getFacebookBoard(self, fb_id, since, until):
    page = ProviderPage.objects.get(id = fb_id)
    token = Token.objects.get(id = page.token_id)

    #message = post_message.encode('utf8')
    msg = {'access_token':token.token, 'since':since, 'until':until }
    url = "https://graph.facebook.com/v2.8/{0}/insights/page_views_total".format(page.page_provider_id)
    page_views = requests.get(url, params=msg)

    url = "https://graph.facebook.com/v2.8/{0}/insights/page_fan_adds".format(page.page_provider_id)
    page_fan_likes = requests.get(url, params=msg)

    url = "https://graph.facebook.com/v2.8/{0}/insights/page_consumptions".format(page.page_provider_id)
    page_engagement = requests.get(url, params=msg)

    url = "https://graph.facebook.com/v2.8/{0}/insights/page_impressions".format(page.page_provider_id)
    page_impressions = requests.get(url, params=msg)


    frm = datetime.fromtimestamp(since)
    to = datetime.fromtimestamp(until)

    result = {
      'page':page.as_base_dict(),
      'page_views':self.getGraphDataFromFBResultset(page_views.json()),
      'page_fan_likes':self.getGraphDataFromFBResultset(page_fan_likes.json()),
      'page_engagement':self.getGraphDataFromFBResultset(page_engagement.json()),
      'page_impressions':self.getGraphDataFromFBResultset(page_impressions.json()),
      'from':datetime.strftime(frm, "%Y-%m-%d"),
      'to':datetime.strftime(to, "%Y-%m-%d")
    }


    return rh.success(result, 'Success')

  def getGraphTotalFromFBResultset(self, res):
    total = 0
    for elem in res['data'][0]['values']:
      if 'value' in elem:      
        total = total + elem['value']

    return total

  def getGraphDataFromFBResultset(self, res):
    result = { 'x':[], 'y':[], 'total':0 }
    for elem in res['data'][0]['values']:
      if 'value' in elem:      
        dt = datetime.strptime(elem['end_time'],'%Y-%m-%dT%H:%M:%S+0000')
        result['x'].append(dt.strftime('%d %B %Y'))
        result['y'].append(elem['value'])
        result['total'] = result['total'] + elem['value']

    return result
     
    #    return HttpResponse(r)
    #
    #
    #    graph = facebook.GraphAPI(access_token=token.token, version='2.7')
    #    pp.pprint(token.token)
    #
    #    profile = graph.get_object(page.page_provider_id)
    #    pp.pprint(profile)
    #    data = profile
    #return rh.success(r, 'Success')





  def getCallLogs(self, venue_id, page_no):
    calls_list = CallLogs.objects.filter(business_id=venue_id).all()
    paginator = Paginator(calls_list, 10)
    calls = paginator.page(page_no)
    
    missed_calls_list = CallLogs.objects.filter(business_id=venue_id, call_transfer_status='Missed').all()
    connected_calls_list = CallLogs.objects.filter(business_id=venue_id, call_transfer_status='Connected').all()

    data = {}
    data['calls'] = [ obj.as_dict() for obj in calls ]
    data['total'] = len(calls_list)
    data['missed'] = len(missed_calls_list)
    data['connected'] = len(connected_calls_list)

    message = "Success"
    return rh.success(data, message)

  def confirmUser(self, _id, name, password):
    usr = Users.objects.get(pk=_id)
    if usr.confirmed == 1:
      return rh.error({}, 'Sorry but this account has already been confirmed.')

    #usr.name = values.name
    #usr.email = values.name
    passwordBytes = password.encode('utf-8')
    hashedPassword = bcrypt.hashpw(passwordBytes, bcrypt.gensalt(4))

    usr.password = hashedPassword
    usr.confirmed = 1

    usr.save()
    data = usr.as_dict()
    message = "Success! You can now log in with your new password."
    return rh.success(data, message)


############################### Login User #################################

  def login(self, emailAddress, password):

    usr = Users.objects.filter(email=emailAddress).first()
    if usr == None:
      message = "Credentials do not match any existing user"
      return rh.error(None, message)

    passwordBytes = password.encode('utf-8')
    hashedPassword = bcrypt.hashpw(passwordBytes, bcrypt.gensalt(4))

    pp.pprint(usr.password.encode('utf-8'))
    pp.pprint(hashedPassword)
    pp.pprint(bcrypt.hashpw(password.encode('utf-8'), usr.password.encode('utf-8')))

    if bcrypt.hashpw(password.encode('utf-8'), usr.password.encode('utf-8')) != usr.password.encode('utf-8'):
      message = "Credentials do not match any existing user"
      return rh.error(None, message)
    else:
      user = usr.as_dict()
      usrID = user['id']

      merchant_ids  = MerchantUser.objects.filter(user_id=usrID).values_list('merchant_id', flat=True)
      venue_ids     = BusinessMerchant.objects.filter(merchant_id__in=merchant_ids).values_list('business_id', flat=True)
      venues        = Businesses.objects.filter(id__in=venue_ids)
      merchants     = Merchants.objects.filter(id__in=merchant_ids)
      facebook_pages = ProviderPage.objects.filter(merchant_id__in=merchant_ids)

      data = {'user': user, 'venues': [ obj.as_base_dict() for obj in venues ],
              'merchants': [ obj.as_base_dict() for obj in merchants ],
              'facebook_pages':[obj.as_dict for obj in facebook_pages ] }
      message = "Success - you're in!"
      return rh.success(data, message)

  def forgetPassword(self, emailAddress, password):
    userObject = ''
    try:
      userObject = Users.objects.get(email=emailAddress)
    except:
      status = "S03"
      return rh.emessage('User does not exist', status)

    userObject.password = password
    userObject.save()

    name = userObject.name
    userID = userObject.id
    data = {'userID': userID, 'name':name}
    status = "S02"
    message = "Welcome back "+name
    return rh.success(data, status, message)

  def testUsers(self):
    usrs = Users.objects.first()
    return usrs.as_dict()
