from __future__ import unicode_literals

from django.db import models

# Create your models here.
class BusinessMerchant(models.Model):
  managed = False

  id = models.IntegerField(primary_key=True)
  business_id = models.IntegerField(null=False, blank=False)
  merchant_id = models.IntegerField(null=False, blank=False)

  #business = models.ForeignKey('Businesses')
  #merchant = models.ForeignKey('Merchants')

  class Meta:
    managed = False
    db_table = 'business_merchant'
    app_label = 'BusinessMerchantModel'

class Businesses(models.Model):
  managed = False

  id = models.IntegerField(primary_key=True)
  name = models.CharField(max_length=255)
  description = models.TextField()
  phone = models.TextField(blank=True)
  email = models.TextField(blank=True)
  website = models.CharField(max_length=255)
  geolocated = models.IntegerField()
  zone_id = models.IntegerField(blank=True, null=True)
  geolocation_city = models.CharField(max_length=255)
  geolocation_state = models.CharField(max_length=255)
  geolocation_country = models.CharField(max_length=255)
  geolocation_address = models.CharField(max_length=255)
  created_at = models.DateTimeField()
  updated_at = models.DateTimeField()
  slug = models.CharField(max_length=255, blank=True)
  active = models.CharField(max_length=255)
  facebook = models.CharField(max_length=255)
  twitter = models.CharField(max_length=255)
  instagram = models.CharField(max_length=255)
  google = models.CharField(max_length=255)
  landmark = models.TextField()
  created_by_id = models.IntegerField()
  updated_by_id = models.IntegerField()
  deleted_by_id = models.IntegerField()
  geolocation_longitude = models.FloatField()
  geolocation_latitude = models.FloatField()
  rate_card_count = models.IntegerField()
  image_count = models.IntegerField()
  reviews_count = models.IntegerField()
  favorites_count = models.IntegerField()
  checkins_count = models.IntegerField()
  services_count = models.IntegerField()
  categories_count = models.IntegerField()
  highlights_count = models.IntegerField()
  timings_count = models.IntegerField()
  zone_cache = models.CharField(max_length=255)
  rating_average = models.FloatField()
  meta = models.TextField(blank=True)
  active_deals_count = models.IntegerField()
  total_deals_count = models.IntegerField()
  total_packages_count = models.IntegerField()
  active_packages_count = models.IntegerField()
  total_ratings_count = models.IntegerField()
  cost_estimate = models.IntegerField()
  active_offers_count = models.IntegerField()
  city_id = models.IntegerField(blank=True, null=True)
  ref = models.CharField(max_length=255)
  lot_id = models.CharField(max_length=255)
  has_sample_menu = models.IntegerField()
  has_stock_cover_image = models.IntegerField()
  popularity = models.IntegerField()
  type = models.CharField(max_length=255)
  is_featured = models.IntegerField()
  is_rich = models.IntegerField()
  cover_x_pos = models.IntegerField()
  cover_y_pos = models.IntegerField()
  is_home_service = models.IntegerField()

  class Meta:
    managed = False
    db_table = 'businesses'
    app_label = 'BusinessModel'

  def __str__(self):
    return "%s" %(self.name)

  def __unicode__(self):
    return "%s" %(self.name)

  def get_cover_image_url(self):
    coverImages = Photos.objects.filter(imageable_id = self.id, imageable_type = "Business", type = "cover")
    image_url = ''
    if coverImages.count() > 0:
      image_url = getPhotoURLForPhotoObject(coverImages[0], MEDIUM)
    else:
      coverImages = Photos.objects.filter(imageable_id = self.id, imageable_type = "Business", type = "image")
      if coverImages.count() > 0:
        image_url = getPhotoURLForPhotoObject(coverImages[0],MEDIUM)
      else:
        if not self.has_stock_cover_image == 0:
          image_url = 'http://d187hsnmfgmmaz.cloudfront.net/stock/'+str(self.has_stock_cover_image)+'.jpg'
        else:
          image_url = 'http://d187hsnmfgmmaz.cloudfront.net/stock/'+'1'+'.jpg'
    return image_url

  def as_base_dict(self):
    data = { 'id': self.id, 'name': self.name, 'zone_cache': self.zone_cache }
    return data

  def as_home_dict(self):
    data = {'id':self.id, 'name': self.name, 'slug':self.slug, 'city_id': self.city_id,
            'reviews_count':self.reviews_count, 'total_ratings_count':self.total_ratings_count,
            'zone_cache':self.zone_cache, 'zone_id':self.zone_id, 'rating_average':self.rating_average,
            'cover_image': self.get_cover_image_url()}
    return data







class Merchants(models.Model):
  managed = False

  id = models.IntegerField(primary_key=True)
  name = models.CharField(max_length=255)
  description = models.TextField(blank=True)
  ref = models.CharField(max_length=255)
  created_at = models.DateTimeField()
  updated_at = models.DateTimeField()
  city_id = models.IntegerField()
  email = models.CharField(max_length=255)
  phone = models.CharField(max_length=255)

  class Meta:
    managed = False
    db_table = 'merchants'
    app_label = 'MerchantModel'

  def as_base_dict(self):
    data = { 'id':self.id, 'name': self.name }
    return data

  def __unicode__(self):
    return u"%s" % self.name

class MerchantUser(models.Model):
  managed = False

  id = models.IntegerField(primary_key=True)
  merchant_id = models.IntegerField(blank=False, null=False)
  user_id = models.IntegerField(blank=False, null=False)

  #merchant = models.ForeignKey('Merchants')
  #user = models.ForeignKey('Users')
  role = models.CharField(max_length=255)

  class Meta:
    managed = False
    db_table = 'merchant_user'
    app_label = 'MerchantUserModel'


class Users(models.Model):
  managed = False

  id = models.IntegerField(primary_key=True)

  email = models.CharField(unique=True, max_length=255)
  password = models.CharField(max_length=255)
  confirmation_code = models.CharField(max_length=255, null=True, blank=True)
  remember_token = models.CharField(max_length=255, null=True, blank=True)
  confirmed = models.IntegerField(null=True, blank=True)


  name = models.CharField(max_length=255)
  about = models.TextField(null=True, blank=True)
  gender = models.CharField(max_length=255, null=True, blank=True)
  twitter = models.CharField(max_length=255, null=True, blank=True)
  instagram = models.CharField(max_length=255, null=True, blank=True)
  favorites_count = models.IntegerField(blank=True, null=True)
  slug = models.SlugField(max_length=200, unique=True)
  followers_count = models.IntegerField(default=0, blank=True)
  follows_count = models.IntegerField(default=0, blank=True)
  phone = models.CharField(max_length=255, null=True, blank=True)


  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  class Meta:
    managed = False
    db_table = "users"
    app_label = 'UserModel'


  def as_dict(self):
    return {
      "id": self.id, "email": self.email,"name": self.name,"about": self.about,
      "gender": self.gender,"twitter": self.twitter,"instagram": self.instagram,
      "favorites_count": self.favorites_count,"slug": self.slug,"followers_count": self.followers_count,
      "follows_count": self.follows_count,"phone": self.phone,
      "created_at": str(self.created_at), "updated_at": str(self.updated_at)
    }

  def as_concise_dict(self):
    return {
      "id": self.id, "name": self.name, "photo_url": self.photo_url
    }

  def as_basic_dict(self):
    return {
      "id": self.id, "name": self.name, "type": "user"
    }

  def user_friendly_name(self):
    return "%s - %s" %(self.id, self.name)


class CallLogs(models.Model):
  managed = False

  id = models.IntegerField(primary_key=True)
  dated = models.DateField()
  timed = models.TimeField()
  called_number = models.CharField(max_length=255)
  caller_number = models.CharField(max_length=255)
  caller_duration = models.CharField(max_length=255, blank=True)
  agent_list = models.CharField(max_length=255, blank=True)
  call_connected_to = models.CharField(max_length=255, blank=True)
  call_transfer_status = models.CharField(max_length=255, blank=True)
  call_transfer_duration = models.CharField(max_length=255, blank=True)
  call_recording_url = models.TextField()
  call_start_time = models.DateTimeField()
  call_pickup_time = models.DateTimeField()
  caller_circle = models.CharField(max_length=255, blank=True)
  business_id = models.IntegerField()
  created_at = models.DateTimeField()
  updated_at = models.DateTimeField()
  is_downloaded = models.IntegerField()
  is_lead = models.IntegerField()
  state = models.CharField(max_length=255)
  is_ppl = models.IntegerField()
  media_file_name = models.CharField(max_length=255, blank=True)
  media_file_size = models.IntegerField(blank=True, null=True)
  media_content_type = models.CharField(max_length=255, blank=True)
  media_updated_at = models.DateTimeField(blank=True, null=True)
  invoice_item_id = models.IntegerField()
  class Meta:
    managed = False
    db_table = 'call_logs'
    
  def as_dict(self):
    return {
      'id':self.id, 'dated':str(self.dated), 'timed':str(self.timed), 
      'called_number':self.called_number, 'caller_number':self.caller_number, 
      'caller_duration':self.caller_duration, 'agent_list':self.agent_list,
      'call_connected_to':self.call_connected_to, 'call_transfer_status':self.call_transfer_status,
      'call_transfer_duration':self.call_transfer_duration, 'call_recording_url':self.call_recording_url,
      'media_url': str(self.call_recording_url), 'call_start_time':str(self.call_start_time), 
      'call_pickup_time':str(self.call_pickup_time), 'caller_circle':self.caller_circle, 
      'created_at': str(self.created_at), 'updated_at': str(self.updated_at)
    }

class VirtualNumberAllocations(models.Model):
  managed = False

  id = models.IntegerField(primary_key=True)
  desc = models.CharField(max_length=255)
  business_id = models.IntegerField()
  virtual_number_id = models.IntegerField()
  starts = models.DateTimeField()
  ends = models.DateTimeField()
  state = models.CharField(max_length=255)
  created_at = models.DateTimeField()
  updated_at = models.DateTimeField()
  phone_1 = models.CharField(max_length=255)
  phone_2 = models.CharField(max_length=255)
  phone_3 = models.CharField(max_length=255)
  is_ppl = models.IntegerField()

  class Meta:
    managed = False
    db_table = 'virtual_number_allocations'

class VirtualNumbers(models.Model):
  managed = False
  id = models.IntegerField(primary_key=True)
  body = models.CharField(max_length=255)
  state = models.CharField(max_length=255)
  created_at = models.DateTimeField()
  updated_at = models.DateTimeField()
  city_id = models.IntegerField()

  class Meta:
    managed = False
    db_table = 'virtual_numbers'


class ProviderPage(models.Model):
  managed = False

  id = models.IntegerField(primary_key=True)
  title = models.CharField(max_length=255)
  page_provider = models.CharField(max_length=255)
  page_provider_id = models.IntegerField()
  merchant_id = models.IntegerField()
  token_id = models.IntegerField()
  access_token = models.TextField(blank=True)

  created_at = models.DateTimeField()
  updated_at = models.DateTimeField()

  class Meta:
    managed = False
    db_table = 'provider_pages'

  def as_base_dict(self):
    return {
      'id':self.id, 'title':self.title, 'page_provider':self.page_provider, 
      'page_provider_id':self.page_provider_id
    }


class Token(models.Model):
  managed = False

  id = models.IntegerField(primary_key=True)
  title = models.CharField(max_length=255)
  provider = models.CharField(max_length=255)
  provider_id = models.IntegerField()
  merchant_id = models.IntegerField()
  token = models.TextField(blank=True)
  refresh_token = models.IntegerField()

  created_at = models.DateTimeField()
  updated_at = models.DateTimeField()

  class Meta:
    managed = False
    db_table = 'tokens'

  def as_base_dict(self):
    return {
      'id':self.id, 'title':self.title, 'provider':self.provider, 
      'provider_id':self.provider_id, 'merchant_id':self.merchant_id,
      'token':self.token, 'refresh_token':self.refresh_token,
      'created_at': str(self.created_at), 'updated_at': str(self.updated_at)
    }
