from django.shortcuts import render, HttpResponse, render_to_response, HttpResponseRedirect, redirect
from api.models import *

import json
from api.auth import Auth
from api.dashboard import Dashboard

from django.views.decorators.csrf import ensure_csrf_cookie

from api.response import Response
import pprint
import time
from datetime import datetime, timedelta

pp = pprint.PrettyPrinter(indent=6)

response = Response()
auth = Auth()
dashboard = Dashboard()

def getHomeBoard(request):
  usrID = request.POST['user_id']

  today = datetime.now()
  lastMonth = today - timedelta(days=30)

  if request.POST.get('since'):
    since = datetime.strptime(request.POST.get('since'), "%m/%d/%Y" )
    since = int(time.mktime(since.timetuple()))
  else:
    since = int(time.mktime(lastMonth.timetuple())) #datetime.strptime(request.POST['since'])

  if request.POST.get('until'):
    until = datetime.strptime(request.POST.get('until'), "%m/%d/%Y" )
    until = int(time.mktime(until.timetuple()))
  else:
    until = int(time.mktime(today.timetuple())) #datetime.strptime(request.POST['until'])

  data = dashboard.getHomeBoard(usrID, since, until)
  return data

# Create your views here.
def getFacebookBoard(request):
  page_id = request.POST['page_id']

  today = datetime.now()
  lastMonth = today - timedelta(days=30)

  if request.POST.get('since'):
    since = datetime.strptime(request.POST.get('since'), "%m/%d/%Y" )
    since = int(time.mktime(since.timetuple()))
  else:
    since = int(time.mktime(lastMonth.timetuple())) #datetime.strptime(request.POST['since'])

  if request.POST.get('until'):
    until = datetime.strptime(request.POST.get('until'), "%m/%d/%Y" )
    until = int(time.mktime(until.timetuple()))
  else:
    until = int(time.mktime(today.timetuple())) #datetime.strptime(request.POST['until'])

  data = dashboard.getFacebookBoard(page_id, since, until)
  return data

def getConfirmableUser(request):
  hsh = ''
  if request.POST.get('hash'):
    hsh = request.POST['hash']
  if request.GET.get('hash'):
    hsh = request.GET['hash']

  data = auth.getConfirmableUser(hsh)

  return data

def confirmUser(request):
  _id = ''
  if request.POST.get('id'):
    _id = request.POST['id']
    name = request.POST['name']
    password = request.POST['password']
  if request.GET.get('id'):
    _id = request.GET['id']
    name = request.GET['name']
    password = request.GET['password']

  response = auth.confirmUser(_id, name, password)
  return response

@ensure_csrf_cookie
def loginUser(request):
  emailAddress = ''
  password = ''


  emailAddress = request.POST.get("emailAddress")
  password = request.POST.get("password")
  
  pp.pprint(emailAddress)
  pp.pprint(password)

  data = auth.login(emailAddress, password)
  pp.pprint(data)
  return data

def getCallLogs(request):
  business_id = request.POST.get("business_id")
  page_no = request.POST.get("page_no")
  data = dashboard.getCallLogs(business_id, page_no)
  return data



def testUsers(request):
  pp.pprint(request.GET)
  pp.pprint(request.POST)


  data = auth.testUsers()
  return HttpResponse(json.dumps(data), content_type="application/json")

# HTACCESS
# Header always set Access-Control-Allow-Origin "*"
# Header always set Access-Control-Allow-Headers "Origin, X-Requested-With, Content-Type, Access-Control-Allow-Origin"
# Header always set Access-Control-Allow-Methods "PUT, GET, POST, DELETE, OPTIONS"