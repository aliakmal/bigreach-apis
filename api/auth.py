from django.shortcuts import render, HttpResponse, render_to_response, HttpResponseRedirect
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.db import connection
from api.models import *
import json
import uuid
import urllib
import bcrypt
from api.response import Response

import string

import random
import datetime
import pprint
pp = pprint.PrettyPrinter(indent=4)
rh = Response() #response handler


class Auth(object):

  def getConfirmableUser(self, confirmation_code):
    usr = Users.objects.filter(confirmation_code=confirmation_code).first()
    if usr == None:
      data = usr
      message = ":( No user found, maybe this user has already been confirmed"
      return rh.error(data, message)

    else:
      data = usr.as_dict()
      message = "Success"
      return rh.success(data, message)

  def confirmUser(self, _id, name, password):
    usr = Users.objects.get(pk=_id)
    if usr.confirmed == 1:
      return rh.error({}, 'Sorry but this account has already been confirmed.')

    #usr.name = values.name
    #usr.email = values.name
    passwordBytes = password.encode('utf-8')
    hashedPassword = bcrypt.hashpw(passwordBytes, bcrypt.gensalt(4))

    usr.password = hashedPassword
    usr.confirmed = 1

    usr.save()
    data = usr.as_dict()
    message = "Success! You can now log in with your new password."
    return rh.success(data, message)


############################### Login User #################################

  def login(self, emailAddress, password):

    usr = Users.objects.filter(email=emailAddress).first()
    if usr == None:
      message = "Credentials do not match any existing user"
      return rh.error(None, message)

    passwordBytes = password.encode('utf-8')
    hashedPassword = bcrypt.hashpw(passwordBytes, bcrypt.gensalt(4))

    pp.pprint(usr.password.encode('utf-8'))
    pp.pprint(hashedPassword)
    pp.pprint(bcrypt.hashpw(password.encode('utf-8'), usr.password.encode('utf-8')))

    if bcrypt.hashpw(password.encode('utf-8'), usr.password.encode('utf-8')) != usr.password.encode('utf-8'):
      message = "Credentials do not match any existing user"
      return rh.error(None, message)
    else:
      user = usr.as_dict()
      usrID = user['id']

      merchant_ids  = MerchantUser.objects.filter(user_id=usrID).values_list('merchant_id', flat=True)
      venue_ids     = BusinessMerchant.objects.filter(merchant_id__in=merchant_ids).values_list('business_id', flat=True)
      venues        = Businesses.objects.filter(id__in=venue_ids)
      merchants     = Merchants.objects.filter(id__in=merchant_ids)
      facebook_pages = ProviderPage.objects.filter(merchant_id__in=merchant_ids)
      pp.pprint(facebook_pages)
      data = {'user': user, 'venues': [ obj.as_base_dict() for obj in venues ],
              'merchants': [ obj.as_base_dict() for obj in merchants ],
              'facebook_pages':[obj.as_base_dict() for obj in facebook_pages ] }

      message = "Success - you're in!"
      return rh.success(data, message)

  def forgetPassword(self, emailAddress, password):
    userObject = ''
    try:
      userObject = Users.objects.get(email=emailAddress)
    except:
      status = "S03"
      return rh.emessage('User does not exist', status)

    userObject.password = password
    userObject.save()

    name = userObject.name
    userID = userObject.id
    data = {'userID': userID, 'name':name}
    status = "S02"
    message = "Welcome back "+name
    return rh.success(data, status, message)

  def testUsers(self):
    usrs = Users.objects.first()
    return usrs.as_dict()
